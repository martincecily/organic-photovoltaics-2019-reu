# Welcome!
#
Welcome to our Organic Photovoltaics repository! Here we've documented our progress looking for solar energy solutions over nine weeks this summer. Through molecular dynamics simulations, we hope to determine some of their qualities and whether or not they have potential to make efficient solar cells. More specifically, we've been investigating organic molecules ITICF4, CZTPTZ8FITIC, and ITIC. Feel free to read below for help navigating our work!

# Navigating

### Data 

To view our data for each OPV candidate (ITICF4, CZTPTZ8FITIC, and ITIC), navigate to "ITICF4-data", "CZTPTZ8FITIC-data" and/or "ITIC-data". Inside these directories, you will find grazing incidence x-ray scattering (GIXS) patterns, radial distribution functions (RDFs), self-assembled morphologies, potential energy plots, and an image of the molecule (molecule.png). All of these functions can be compared according to changing temperatures, densities, e factors, and combinations of all three conditions (plus system size and time scale).

### Literature/References

To view all references used over the course of our research, navigate to the "resources" directory. We have included papers, dissertations, images, and websites with more detailed information on our work.

### Raw Code

To view some of the code we used to analyze data, navigate to the "code" directory. Please note that most code is specific to files and directory paths we already have in our systems, so you may need to edit the code if you would like to use it.

### Poster

To view our poster, open "poster.pdf".

# Helpful Terms to Know
#
**Acceptor-Donor-Acceptor (ADA) Molecules:** Molecules with two electron-accepting groups, one on each end, and an electron donor region in the middle. Our molecules are examples of ADA molecules (although we plan on mixing them with a pure electron donor compound, PTB7). The most common organic photovoltaics at this point are fullerene-based acceptor molecules, but while these are fairly efficient, they have large energy losses [Zhao 2017 JACS]. The new ADA molecules are non-fullerene and can form the necessary acceptor and donor regions themselves.

**Charge Transport:** The flow of electrons through a given material. In the context of organic solar cells, charge transport describes the way electrons travel through the bulk heterojunction, or the photovoltaic portion of a solar cell. Specifically, this process occurs when a photon (light particle) is absorbed by an OPV and forms an electron-hole pair called an exciton. Once the exciton reaches an electron acceptor-donor interface, it separates into a free electron and the hole, and the free electron can then travel through the material until it reaches the solar cell's electrode, which conducts electricity in order to generate voltage. Higher charge transport means free electrons are able to be transported and conducted more quickly and in larger amounts. Thus good charge transport is imperative for efficient solar cells. 

**e Factor:** A way of representing how attracted molecules are to each other without changing any other thermodynamic conditions. The e factor represents the "stickiness" of the molecules. We consider the e factor as a parameter controlling an invisible solvent in which our molecules move and self-assemble into morphologies. A low e factor indicates that the solvent interacts with the molecules as little as possible.

**Grazing Incidence X-Ray Scattering (GIXS):** An analysis technique in which x-rays are fired at a sytem and the amount of light that bounces back is tracked through constructive interference in order to make inferences about system structure. We use simulated GIXS to evaluate the ordering of our morphologies. Bright, repeating rings or dots indicate that a system is displaying significant ordering. Please note that GIXS jobs are located in the "diffract" directory for ITIC and are denoted as "adp" and "asq" files for ITIC-F4.   

**Morphology:** The thermodynamically-driven, energy-minimized configuration that molecules assemble into in a given system. A morphology can demonstrate how clumped or spread out molecules are and, more specifically, how molecules stack within a system (if they stack at all). Ultimately, the morphology of the acceptor and donor regions plays a huge role in charge transport. 

**Ordering:** When molecules arrange themselves in ways that are neater and more tightly-packed. There are two types of order, short-range and long-range. Short-range order refers to pi-pi stacking, when molecules stack one on top of another due to chemical interactions between aromatic rings in the molecular structure, which contain pi bonds. Long-range order refers to predictable arrangements that occur throughout a material (like crystalline materials) and often corresponds to a phenomenon called "lamellar ordering". Lamellar ordering occurs when clumps or "ribbons" of molecules layer one on top of another. With certain OPVs (like the compound P3HT), greater ordering corresponds to greater charge transport. The main focus of our study has been trying to determine whether our molecules display significant ordering, since we hypothesized that the high efficiency of solar cells using these materials was a result of ordering in the system.  

**Organic Photovoltaics (OPVs):** Carbon-based materials that are able to convert light, or solar energy, into electricity. These are used to make solar cells that are thinner and more flexible than "traditional", silicon-based solar cells (they also have the potential for cheap production!). 
Our work lies in trying to figure out how to make OPV materials more efficient. In other words, we ultimately want to increase the percentage of solar energy that can be converted into electric current. 

**Radial Distribution Function (RDF):** A representation of the probability that any two molecule are located a certain distance away from each other. For reference, a completely random system of molecules corresponds to an RDF at 1.0. Peaks above 1.0 indicate that molecules are more likely to be that distance apart than in a random system.  We can interpret these peaks as points of order in the system. 

**Self-Assembly:** When simulated molecules arrange into the most thermodynamically-favorable configuration (without having to be told where to go). We use self-assembly so we can get an idea of how the molecules will arrange in a real system.

**Steric Hindrance:** The prevention or slowing of chemical reactions due to bulky arrangements of atoms that make it hard for the given molecules to interact. For example, our molecules are big and bulky and have lots of functional groups, so they are going to be subject to steric hindrance when they try to group together and order. 

**System Size:** The number of molecules in a system when we simulate self-assembled morphologies. Smaller systems typically consist of 100 molecules, larger systems typically use 1000 molecules. 


# Programs Used
#
HOOMD-blue
(https://github.com/glotzerlab/hoomd-blue)

mBuild
(https://github.com/mosdef-hub/mbuild)

MorphCT
(https://github.com/matty-jones/MorphCT)

PlanckTon
(https://bitbucket.org/cmelab/planckton/src/dev/)

Signac
(https://docs.signac.io/en/latest/)

VMD
(https://www.ks.uiuc.edu/Research/vmd/)


# Acknowledgements
#
Thank you to our mentor, Dr. Eric Jankowski, and to labmates Neale Ellyson, Jenny Fothergill, Mike Henry, Chris Jones, Dr. Evan Miller, and Dr. Matty Jones for their help and guidance.

Thank you to the National Science Foundation for providing the funding and opportunities to learn through the BSU Materials Science Summer 2019 REU program.

For more information on the BSU Computational Materials Engineering lab, visit https://bitbucket.org/cmelab/profile/repositories.

# Contact
#
Cecily Martin, CZTPTZ8FITIC: cem204@miami.edu

Nate Schwindt, ITIC-F4: schwinns@rose-hulman.edu

Mia Klopfenstein, ITIC: miaklopfenstein@u.boisestate.edu

Dr. Eric Jankowski (PI): ericjankowski@boisestate.edu
