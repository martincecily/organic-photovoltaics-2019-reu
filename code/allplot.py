import signac
import numpy as np
import matplotlib.pyplot as plt
import os
from matplotlib.font_manager import FontProperties as fp


data_path = "../planckton-flow/" #path to flow directory
project = signac.get_project(data_path)
path = "../planckton-flow/view/"
statepoint = "kT_reduced/"

### Asking if you have a view folder for your planckton jobs

''' 
if path == False:
    os.chdir(data_path)
    os.system("signac view")
    os.chdir("../signac-space/")
    print("Created a view folder.")


if os.path.isdir(path) == True:
    print("View is already created.")

### The below line to group your data

for key, group in project.groupby("n_compounds"):
        print(key, list(group))
'''


x = np.arange(10)
fig = plt.figure()
plt.xlabel('Timestep')
plt.ylabel('Potential Energy')
plt.title('PE at Different Temps')
ax = plt.subplot(111)

box = ax.get_position()
ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

'''
for k, g in project.groupby('kT_reduced'):
    #for job in project:
    #    print(job)

    if k == 2.38:
        print(list(g))
        #print(list(g))

'''
for job in project:
    data_file = job.fn("trajectory.log") # finds the trajectory file
    data = np.genfromtxt(data_file)
    x, y = data[2000:,0], data[2000:,4]
    dictionary = project.get_statepoint(job.get_id())
    label = dictionary.get('kT_reduced')
    plt.plot(x, y, label=label)

ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))#plt.legend(loc='right', fontsize='small')

plt.savefig('plots/itic/all_temp.pdf')

