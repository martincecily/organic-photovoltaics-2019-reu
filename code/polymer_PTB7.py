# Polymerizes PTB7. To use, type "python polymer_PTB7.py #" with # indicating how many monomers you wish to link together. 

import mbuild as mb
import numpy as np
import random
import argparse

# Load in the monomer PTB7.pdb for bond references
withH = mb.load("PTB7.pdb")
left_port = (
    withH[5].pos - withH[0].pos
)  # vector of coordinates for the left port (from anchored carbon)
right_port = (
    withH[52].pos - withH[47].pos
)  # vector of coordinates for the right port (from anchored carbon)
left_H_pos = (
    withH[5].pos - withH[0].pos
)  # vector of coordinates for the left hydrogen atom
right_H_pos = (
    withH[52].pos - withH[0].pos
)  # vector of coordinates for the right hydrogen atom
poly_port = (
    withH[38].pos - withH[3].pos
)  # vector of coordinates for the carbon-carbon bond between repeat units


class base(mb.Compound):
    def __init__(self):
        super(base, self).__init__()

        mb.load(
            "PTB7_no_H.pdb", compound=self
        )  # load in .pdb to use as the base repeat unit
        mb.Compound.translate(self, -self[0].pos)

        # self.remove(self[5])
        # self.remove(self[51])  # remove the hydrogens at the polymerizing sites

        self.add(
            mb.Port(anchor=self[0]), label="left"
        )  # anchoring the ports to the appropriate carbons for polymerization
        self.add(
            mb.Port(anchor=self[46]), label="right"
        )  # carbon 46 is taking into account the removal of the hydrogens from above

        mb.Compound.translate(
            self["left"], left_port / np.linalg.norm(left_port) * poly_port / 2
        )  # direction of left_port with the magnitude of half poly_port
        mb.Compound.translate(
            self["right"], right_port / np.linalg.norm(right_port) * poly_port / 2
        )


class PTB7Polymer(mb.Compound):
    def __init__(self, chain_length):
        super(PTB7Polymer, self).__init__()
        monomer_proto = base()
        last_monomer = base()
        self.add(last_monomer)
        shift_pos_right = 0
        for i in range(chain_length - 1):
            current_monomer = mb.clone(monomer_proto)
            mb.force_overlap(
                move_this=current_monomer,
                from_positions=current_monomer["left"],
                to_positions=last_monomer["right"],
            )
            self.add(current_monomer)
            shift_pos_right = (
                current_monomer[46].pos - self[46].pos
            )  # vector of coordinates that indicate how far the current monomer is shifted from the original position
            last_monomer = current_monomer

        self.add(mb.Particle(name="H", pos=left_H_pos), label="H_left")
        self.add(
            mb.Particle(name="H", pos=right_H_pos + shift_pos_right), label="H_right"
        )

        self.add_bond((self[0], self["H_left"]))
        self.add_bond(
            (self[46 + (chain_length - 1) * 103], self["H_right"])
        )  # adds a bond to the last right H in the polymer chain - because each repeat unit has 103 atoms

<<<<<<< HEAD
        self.energy_minimize(forcefield='GAFF', steps=2000) # minimizes the energy of the molecule and removes overlapping of chains
=======
        self.energy_minimize(forcefield='GAFF', steps = 2000) # minimizes the energy of the molecule and removes overlapping of chains
>>>>>>> 5bb7a2fa322f186e4af4fce3e7d598467e3c81ae

def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )

    parser.add_argument(
        "-l",
        "--length",
        type=int,
        default=2,
        required=False,
        help="Chain length of the polymer",
    )

    parser.add_argument("--noH", type=str2bool, nargs='?',
                                const=True, default=False,
                                                        help="Removes all hydrogens from polymer.")

    args = parser.parse_args()

    ex = PTB7Polymer(args.length)

    if args.noH:
        hydrogens = ex.particles_by_name('H')
        for i in hydrogens:
            ex.remove(i)
    
    filename = "PTB7_{}mer.mol2".format(args.length)
    ex.save(filename, overwrite=True)
