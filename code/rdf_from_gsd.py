# Generates RDF plots from gsd files (e.g. VMD morphology files). To use, type "python rdf_from_gsd.py <file>" with <file> indicating the gsd file you wish to use.

import gsd.fl
import gsd.hoomd
import gsd.pygsd
import numpy as np
import hoomd
import freud
from matplotlib import pyplot as plt
import sys
import os

def gsd_read(planckton_job):
    #file = planckton_job+'job/trajectory.gsd'
    file = planckton_job
    f = gsd.pygsd.GSDFile(open(file, 'rb'))
    t = gsd.hoomd.HOOMDTrajectory(f)

    frame_size = len(t[0].particles.position)
    num_frames = len(t)
    num_atoms = 186-82
    num_molecules = frame_size//num_atoms

    return t, frame_size, num_frames, num_atoms, num_molecules, planckton_job
    
def no_pbc(t, frame_size, num_frames, num_atoms, num_molecules):
    centers = np.empty((num_frames,num_molecules,3))

    for i in range(num_frames):
        #print('Frame for centers:',i)
        data = t[i].particles.position
        L = t[i].configuration.box[0]
        
        for n in range(0,num_molecules):
            molecule_new = np.empty((num_atoms,3))
            molecule = data[num_atoms*n:num_atoms*(n+1),:]
            for b in range(num_atoms):
                dim_changed = []
                dim_unchanged = np.empty((2,1))
                check = np.linalg.norm(molecule[0,:]-molecule[b,:])
                check0 = np.linalg.norm(molecule[0,0]-molecule[b,0])
                check1 = np.linalg.norm(molecule[0,1]-molecule[b,1])
                check2 = np.linalg.norm(molecule[0,2]-molecule[b,2])
                if  check >= L/2:
                    if check0 >= check1 and check0 >= check2:
                        molecule_new[b,0] = molecule[b,0] - L*np.sign(molecule[b,0])
                        molecule_new[b,1] = molecule[b,1]
                        molecule_new[b,2] = molecule[b,2]
                        dim_changed = 0 
                        dim_unchanged = np.array([[1],[2]])
                    elif check1 >= check0 and check1 >= check2:
                        molecule_new[b,1] = molecule[b,1] - L*np.sign(molecule[b,1])
                        molecule_new[b,0] = molecule[b,0]
                        molecule_new[b,2] = molecule[b,2]
                        dim_changed = 1 
                        dim_unchanged = np.array([[0],[2]])
                    elif check2 >= check0 and check2 >= check1:
                        molecule_new[b,2] = molecule[b,2] - L*np.sign(molecule[b,2])
                        molecule_new[b,0] = molecule[b,0]
                        molecule_new[b,1] = molecule[b,1]
                        dim_changed = 2 
                        dim_unchanged = np.array([[0],[1]])
                else:
                    molecule_new[b,:] = molecule[b,:]
                new_check = np.linalg.norm(molecule[0,:]-molecule_new[b,:])
                if  new_check >= L/2:
                    new_check0 = np.linalg.norm(molecule_new[0,dim_unchanged[0]]-molecule_new[b,dim_unchanged[0]])
                    new_check1 = np.linalg.norm(molecule_new[0,dim_unchanged[1]]-molecule_new[b,dim_unchanged[1]])
                    if new_check0 >= new_check1:
                        molecule_new[b,dim_unchanged[0]] = molecule_new[b,dim_unchanged[0]] - L*np.sign(molecule_new[b,dim_unchanged[0]])
                        q = 1
                    else:
                        molecule_new[b,dim_unchanged[1]] = molecule_new[b,dim_unchanged[1]] - L*np.sign(molecule_new[b,dim_unchanged[1]])
                        q = 0
                    new_check = np.linalg.norm(molecule_new[0,:]-molecule_new[b,:])
                    if new_check >= L/2:
                        molecule_new[b,dim_unchanged[q]] = molecule_new[b,dim_unchanged[q]] - L*np.sign(molecule_new[b,dim_unchanged[q]])
                    
            centers[i,n,:] = np.mean(molecule_new, axis=0)
            
    return centers

def pbc_centers(t, num_frames, num_molecules, centers):
    centers_shift = np.empty((num_frames, num_molecules, 3))
    for i in range(num_frames):
        L = t[i].configuration.box[0]
        for n in range(num_molecules):
            for dim in range(3):
                if np.absolute(centers[i,n,dim]) >= L/2:
                    centers_shift[i,n,dim] = centers[i,n,dim] - np.sign(centers[i,n,dim])*L
                else:
                    centers_shift[i,n,dim] = centers[i,n,dim]

    return centers_shift

def generate_rdf(t, num_frames, centers_shift, planckton_job):
    boxes = [t[i].configuration.box[0] for i in range(len(t))]
    rdf = freud.density.RDF(rmax=6, dr=0.1)

    for i in range(100, num_frames):
        bbox = t[i].configuration.box
        box = freud.box.Box(Lx=bbox[0], Ly=bbox[1], Lz = bbox[2]) 
        rdf.accumulate(box, centers_shift[i,:,:])

    fig = plt.figure()
    plt.plot(rdf.R,rdf.RDF)
    plt.xlabel("r")
    plt.ylabel("g(r)")
    plt.savefig(planckton_job+"rdf.png")


if __name__ == "__main__":

    t, frame_size, num_frames, num_atoms, num_molecules, planckton_job = gsd_read(sys.argv[1])
    centers = no_pbc(t, frame_size, num_frames, num_atoms, num_molecules)
    centers_shift = pbc_centers(t, num_frames, num_molecules, centers)
    generate_rdf(t, num_frames, centers_shift, planckton_job)
