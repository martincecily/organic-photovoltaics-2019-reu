# This piece of code is used to sort through morphologies of jobs with a combination of factors changed.


import glob
import re
from IPython.display import Image, display

for image in sorted(glob.glob('./n*den*')): # Combing through files in current directory to find job images with "n" and "den" in name (this indicates a combination job; n represents number molecules)
    display(Image(filename=image, width=200, height=200)) # Controls image size
    image_float = re.findall(r'-?\d+\.?\d*', image) # Extracting density, e factor, temp, etc. numbers from image name to display further down
    last = image_float[5]
    print("Number Molecules = ['" + image_float[0] + "']") 
    print("Density = ['" + image_float[1] + "']") 
    print("e Factor = ['" + image_float[2] + "']")
    print("Temperature = ['" + image_float[3] + "']")
    print("Timesteps = ['" + "1e" + last[:-1] + "']") # Wanted this to be an integer instead of a float, so I simply removed the decimal at the end of the number
    
    