# This piece of code is used to sort through morphologies of jobs where density or e factor values have been changed.


import glob
import re
from IPython.display import Image, display

for image in sorted(glob.glob('/Users/cecilymartin/Projects/den-runs/den*/diffract/adp.png')): # Looking for job images with "den" in the title. For e factor, I simply changed "den*" to "efact*".
    display(Image(filename=image, width=200, height=200)) # Controls image size
    image_float = re.findall(r'-?\d+\.?\d*', image) # Extracting value from title to display below
    print("Density =", image_float) # For e factor, I simply changed "Density =" to "e Factor ="